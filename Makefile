SRC = ./src
BUILD = ./build
TARGET = /usr/bin/ndh-launcher

prepare:
	mkdir -p $(BUILD)

build: prepare
	cd $(BUILD) && qmake .$(SRC) && make
	mv $(BUILD)/src $(BUILD)/ndh-launcher

clean:
	cd $(BUILD) && make clean
	rm -rf $(BUILD)*

install:
	mv $(BUILD)/ndh-launcher $(TARGET)
