#include "mainwindow.h"

#include <filesystem>
#include <fstream>
#include <iostream>

#include <QPushButton>
#include <QHBoxLayout>
#include <QScrollArea>

std::string *listDir(char *path) {
  std::string *ret;
  size_t size = 0;

  for (const auto &entry : std::filesystem::directory_iterator(path)) {
    //ret[size] = (char *) ((std::string) entry.path()).c_str(); // FIXME: this line causes core dump
    ret[size] = (std::string) entry.path();
    size++;
  }
  return ret;
}

char **read(char *file) {
  std::ifstream _read(file);
  char **ret;
  size_t size = 0;
  std::string text;

  while (getline(_read, text)) {
    ret[size] = (char *) text.c_str();
    size++;
  }

  return ret;
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
  addApps();
}

void MainWindow::addApps() {
  // FIXME
  //std::string *ls = listDir("/usr/share/applications/");

  QHBoxLayout *hboxlayout = new QHBoxLayout();
  hboxlayout->setSizeConstraint(QLayout::SetMaximumSize);

  //QScrollArea *scrollarea = new QScrollArea();
  //scrollarea->setBackgroundRole(QPalette::Shadow);

  //hboxlayout->addWidget(scrollarea);
  //scrollarea->setWidget(this);

  for (int i = 0; i < /*sizeof(ls)*/ 20; i++) {
    QPushButton *button = new QPushButton();
    button->setText("test");  // Only for testing

    hboxlayout->addWidget(button);
  }

  this->setLayout(hboxlayout);  // FIXME
}

MainWindow::~MainWindow()
{
}

