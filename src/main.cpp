#include "mainwindow.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QGraphicsBlurEffect>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  QDesktopWidget dw;
  w.setFixedSize(300, 350);
  w.setWindowFlags(Qt::CustomizeWindowHint);
  w.setWindowFlags(Qt::WindowStaysOnTopHint);
  w.move(0, dw.height() - dw.height()/20);

  QGraphicsBlurEffect *blur = new QGraphicsBlurEffect;

  blur->setBlurRadius(10);
  blur->setBlurHints(QGraphicsBlurEffect::QualityHint);

  w.setGraphicsEffect(blur);

  w.setAttribute(Qt::WA_NoSystemBackground, true);
  w.setAttribute(Qt::WA_TranslucentBackground, true);

  w.show();  // Only for testing
  return a.exec();
}
